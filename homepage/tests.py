from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.utils import timezone
from .views import index
from .apps import HomepageConfig

class Story6UnitTest(TestCase):

    def test_story_6_url_is_exist(self):
    	response = Client().get('/')
    	self.assertEqual(response.status_code, 200)
    
    def test_story6_using_status_func(self):
    	found = resolve('/')
    	self.assertEqual(found.func, index)
    
    def test_story6_using_status_template(self):
        response = Client().get('/status/')
        self.assertTemplateUsed(response, 'homepage.html')